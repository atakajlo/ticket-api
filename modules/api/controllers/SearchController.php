<?php

namespace app\modules\api\controllers;


use app\models\Flight;
use app\models\SearchModel;
use yii\filters\auth\HttpBasicAuth;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;

class SearchController extends Controller
{
    protected function verbs()
    {
        return [
            'index' => ['POST']
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
//        $behaviors['authenticator']['class'] = HttpBasicAuth::class;
//        $behaviors['authenticator']['auth'] = function ($username, $password) {
//            $user = \app\models\User::findByUsername($username);
//            if ($user->validatePassword($password)) {
//                return $user;
//            }
//            return null;
//        };
        return $behaviors;
    }

    public function actionIndex()
    {
        $model = new SearchModel();
        $model->load(\Yii::$app->request->post(), '');

        if ($model->validate()) {
            $flights = Flight::find()
                ->where([
                    'airport_from' => $model->airportFrom,
                    'airport_to' => $model->airportTo,
                ])
                ->andWhere(['between', 'date_begin', $model->date, $model->date . ' 23:59:59'])
                ->all();

            if (count($flights) == 0) {
                throw new NotFoundHttpException('Nothing found');
            }

            return [
                'searchQuery' => $model,
                'searchResults' => $flights,
            ];
        }

        return $model;
    }
}