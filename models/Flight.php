<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%flight}}".
 *
 * @property int $id
 * @property int $airport_from
 * @property int $airport_to
 * @property int $number
 * @property string $date_begin
 * @property string $date_end
 * @property string $transport_id
 *
 * @property Airport $airportFrom
 * @property Airport $airportTo
 * @property Transport $transport
 */
class Flight extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%flight}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['airport_from', 'airport_to', 'number'], 'integer'],
            [['date_begin', 'date_end'], 'safe'],
            [['airport_from'], 'exist', 'skipOnError' => true, 'targetClass' => Airport::className(), 'targetAttribute' => ['airport_from' => 'id']],
            [['airport_to'], 'exist', 'skipOnError' => true, 'targetClass' => Airport::className(), 'targetAttribute' => ['airport_to' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'airport_from' => 'Airport From',
            'airport_to' => 'Airport To',
            'number' => 'Number',
            'date_begin' => 'Date Begin',
            'date_end' => 'Date End',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAirportFrom()
    {
        return $this->hasOne(Airport::className(), ['id' => 'airport_from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAirportTo()
    {
        return $this->hasOne(Airport::className(), ['id' => 'airport_to']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransport()
    {
        return $this->hasOne(Transport::class, ['id' => 'transport_id']);
    }

    public function fields()
    {
        return [
            'transporter' => function(self $model) {
                return $model->transport;
            },
            'flightNumber' => 'number',
            'departureAirport' => function(self $model) {
                return $model->airportFrom->code;
            },
            'arrivalAirport' => function(self $model) {
                return $model->airportTo->code;
            },
            'departureDateTime' => 'date_begin',
            'arrivalDateTime' => 'date_end',
            'duration' => function(self $model) {
                $start = new \DateTime($model->date_begin);
                $end = new \DateTime($model->date_end);
                $diff = $end->diff($start);
                return $diff->h * 60 + $diff->i;
            }
        ];
    }
}
