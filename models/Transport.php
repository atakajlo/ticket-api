<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%transport}}".
 *
 * @property int $id
 * @property string $code
 * @property string $name
 */
class Transport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%transport}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code'], 'string', 'max' => 2],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
        ];
    }

    public function fields()
    {
        $fields = parent::fields();
        unset($fields['id']);
        return $fields;
    }
}
