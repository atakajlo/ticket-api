<?php

namespace app\models;


use yii\base\Model;

class SearchModel extends Model
{
    public $airportFrom;
    public $airportTo;
    public $date;

    public function rules()
    {
        return [
            [['airportFrom', 'airportTo', 'date'], 'required'],
            [['airportFrom', 'airportTo'], 'integer'],
            [['airportFrom'], 'compare', 'compareAttribute' => 'airportTo', 'operator' => '!='],
            ['date', 'date', 'format' => 'php:Y-m-d'],
        ];
    }

    public function attributeHints()
    {
        return [
            'date' => 'Format like 2018-09-23'
        ];
    }
}