<?php

namespace app\fixtures;

use yii\test\ActiveFixture;

class AirportFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Airport';
    public $dataFile = '@tests/unit/fixtures/data/airport.php';
}