<?php

namespace app\fixtures;

use yii\test\ActiveFixture;

class FlightFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Flight';
    public $dataFile = '@tests/unit/fixtures/data/flight.php';
    public $depends = [
        AirportFixture::class,
        TransporterFixture::class,
    ];
}