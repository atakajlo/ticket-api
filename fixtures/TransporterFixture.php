<?php

namespace app\fixtures;

use yii\test\ActiveFixture;

class TransporterFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Transport';
    public $dataFile = '@tests/unit/fixtures/data/transporter.php';
}