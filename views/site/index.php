<?php

/* @var $this yii\web\View */
/* @var $model app\models\SearchModel */
/* @var $dataProvider \yii\data\ArrayDataProvider */

use yii\widgets\ActiveForm;

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <?php $form = ActiveForm::begin() ?>

    <?= $form->field($model, 'airportFrom')->dropDownList($airportList) ?>
    <?= $form->field($model, 'airportTo')->dropDownList($airportList) ?>
    <?= $form->field($model, 'date') ?>

    <?= \yii\helpers\Html::submitButton('Search', ['class' => 'btn btn-success']) ?>

    <?php ActiveForm::end() ?>

    <br>

    <?= \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'flightNumber',
            [
                'attribute' => 'transporter',
                'value' => function($item) {
                    return $item['transporter']['code'] . ' - ' . $item['transporter']['name'];
                }
            ],
            'departureAirport',
            'arrivalAirport',
            'departureDateTime',
            'arrivalDateTime',
            'duration'
        ]
    ]) ?>
</div>
