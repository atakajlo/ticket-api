<?php

namespace app\bootstrap;


use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\httpclient\Client;

class SetUp implements BootstrapInterface
{
    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        $container = \Yii::$container;

        $container->set(Client::class, function() {
            return new Client([
                'baseUrl' => 'http://php/'
            ]);
        });
    }
}