### Installation

```
composer install
docker-compose up --build -d
docker-compose run php php yii migrate
docker-compose run php php yii fixture/load '*' --namespace='app\fixtures'
```

```
open http://localhost:8000
```