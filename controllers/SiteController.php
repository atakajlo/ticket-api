<?php

namespace app\controllers;

use app\models\Airport;
use app\models\SearchModel;
use yii\base\Module;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use yii\web\Controller;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @var Client
     */
    private $client;

    public function __construct(string $id, Module $module, Client $client, array $config = [])
    {
        $this->client = $client;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new SearchModel();
        $airportList = ArrayHelper::map(Airport::find()->all(), 'id', 'code');
        $data = [];

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $response = $this->client->createRequest()
                ->setMethod('POST')
                ->setUrl('api/search')
                ->setData($model->toArray())
                ->send();

             if (isset($response->data['searchResults'])) {
                 $data = $response->data['searchResults'];
             }
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data
        ]);

        return $this->render('index', [
            'model' => $model,
            'airportList' => $airportList,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
