<?php

use yii\db\Migration;

/**
 * Handles the creation of table `airport`.
 */
class m180923_113727_create_airport_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%airport}}', [
            'id' => $this->primaryKey(),
            'code' => $this->string(3),
            'name' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%airport}}');
    }
}
