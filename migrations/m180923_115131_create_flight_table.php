<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%flight}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%airport}}`
 * - `{{%airport}}`
 */
class m180923_115131_create_flight_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%flight}}', [
            'id' => $this->primaryKey(),
            'airport_from' => $this->integer(),
            'airport_to' => $this->integer(),
            'transport_id' => $this->integer(),
            'number' => $this->string(),
            'date_begin' => $this->datetime(),
            'date_end' => $this->datetime(),
        ]);

        // creates index for column `airport_from`
        $this->createIndex(
            '{{%idx-flight-airport_from}}',
            '{{%flight}}',
            'airport_from'
        );

        // add foreign key for table `{{%airport}}`
        $this->addForeignKey(
            '{{%fk-flight-airport_from}}',
            '{{%flight}}',
            'airport_from',
            '{{%airport}}',
            'id',
            'CASCADE'
        );

        // creates index for column `airport_to`
        $this->createIndex(
            '{{%idx-flight-airport_to}}',
            '{{%flight}}',
            'airport_to'
        );

        // add foreign key for table `{{%airport}}`
        $this->addForeignKey(
            '{{%fk-flight-airport_to}}',
            '{{%flight}}',
            'airport_to',
            '{{%airport}}',
            'id',
            'CASCADE'
        );

        // creates index for column `transport_id`
        $this->createIndex(
            '{{%idx-flight-transport_id}}',
            '{{%flight}}',
            'transport_id'
        );

        // add foreign key for table `{{%transport}}`
        $this->addForeignKey(
            '{{%fk-flight-transport_id}}',
            '{{%flight}}',
            'airport_from',
            '{{%transport}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%airport}}`
        $this->dropForeignKey(
            '{{%fk-flight-transport_id}}',
            '{{%flight}}'
        );

        // drops index for column `airport_to`
        $this->dropIndex(
            '{{%idx-flight-transport_id}}',
            '{{%flight}}'
        );

        // drops foreign key for table `{{%airport}}`
        $this->dropForeignKey(
            '{{%fk-flight-airport_from}}',
            '{{%flight}}'
        );

        // drops index for column `airport_from`
        $this->dropIndex(
            '{{%idx-flight-airport_from}}',
            '{{%flight}}'
        );

        // drops foreign key for table `{{%airport}}`
        $this->dropForeignKey(
            '{{%fk-flight-airport_to}}',
            '{{%flight}}'
        );

        // drops index for column `airport_to`
        $this->dropIndex(
            '{{%idx-flight-airport_to}}',
            '{{%flight}}'
        );

        $this->dropTable('{{%flight}}');
    }
}
