<?php

use yii\db\Migration;

/**
 * Handles the creation of table `transport`.
 */
class m180923_113548_create_transport_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%transport}}', [
            'id' => $this->primaryKey(),
            'code' => $this->string(2),
            'name' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%transport}}');
    }
}
