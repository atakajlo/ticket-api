<?php

return [
    'airport0' => [
        'code' => 'ZOD',
        'name' => 'Airport 1',
    ],
    'airport1' => [
        'code' => 'ZYR',
        'name' => 'Airport 2',
    ],
    'airport2' => [
        'code' => 'ZAE',
        'name' => 'Airport 3',
    ],
    'airport3' => [
        'code' => 'LOV',
        'name' => 'Airport 4',
    ],
    'airport4' => [
        'code' => 'WNT',
        'name' => 'Airport 5',
    ],
];
