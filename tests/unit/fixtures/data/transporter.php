<?php

return [
    'transporter0' => [
        'code' => 'SR',
        'name' => 'Transporter 1',
    ],
    'transporter1' => [
        'code' => 'RP',
        'name' => 'Transporter 2',
    ],
    'transporter2' => [
        'code' => 'ZA',
        'name' => 'Transporter 3',
    ],
    'transporter3' => [
        'code' => 'SW',
        'name' => 'Transporter 4',
    ],
    'transporter4' => [
        'code' => 'NI',
        'name' => 'Transporter 5',
    ],
];
