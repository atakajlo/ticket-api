<?php
/**
 * @var $faker \Faker\Generator
 * @var $index integer
 */

$airportRange = range(1, 5);
$airportFrom = $faker->randomElement($airportRange);

if ($key = array_search($airportFrom, $airportRange)) {
    unset($airportRange[$key]);
}
$airportTo = $faker->randomElement($airportRange);

return [
    'number' => $faker->randomElement(range('A', 'Z')) . $faker->randomNumber(4),
    'date_begin' => $faker->dateTimeBetween('-2 hours', 'now')->format('Y-m-d H:i:00'),
    'date_end' => $faker->dateTimeBetween('now', '+2 hours')->format('Y-m-d H:i:00'),
    'airport_from' => $airportFrom,
    'airport_to' => $airportTo,
    'transport_id' => $faker->randomElement(range(1, 5)),
];