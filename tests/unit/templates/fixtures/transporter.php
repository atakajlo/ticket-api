<?php
/**
 * @var $faker \Faker\Generator
 * @var $index integer
 */

return [
    'code' => implode("", $faker->randomElements(range('A', 'Z'), 2)),
    'name' => 'Transporter ' . ($index + 1)
];