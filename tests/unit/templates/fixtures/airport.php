<?php
/**
 * @var $faker \Faker\Generator
 * @var $index integer
 */

return [
    'code' => implode("", $faker->randomElements(range('A', 'Z'), 3)),
    'name' => 'Airport ' . ($index + 1)
];
